/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

/*
   Ask the user if wants to save or not the current note before closing or replacing it
*/
Dialog {
    id: confirmSaveDialogue
    title: i18n.tr("Confirmation")
    text: i18n.tr("There are unsaved changes in the current note. What do you want to do?")

    Column {
        spacing: units.gu(3)

        /* user want save note content: save current note and proceed then*/
        Button {
            text: i18n.tr("Go back")
            color: theme.palette.normal.positive
            width: parent.width
            height: units.gu(5)
            onClicked: PopupUtils.close(confirmSaveDialogue)
        }

        /* user DOESN'T want to save old note: loose unsaved content */
        Button {
            // use a label instead of text property because that doesn't allow wordwrap
            Label {
                id: buttonLabel
                anchors {
                    centerIn: parent
                    margins: units.gu(1)
                }
                width: parent.width - units.gu(2)
                text: i18n.tr("Drop changes and proceed")
                wrapMode: Text.Wrap
                horizontalAlignment: Text.AlignHCenter
                // hardcoded button colors depending on theme https://gitlab.com/ubports/development/core/lomiri-ui-toolkit/-/blob/main/src/imports/Components/Themes/Ambiance/1.3/ButtonStyle.qml#L170
                color: (theme.name == "Lomiri.Components.Themes.Ambiance") ? "#111111" : "#FFFFFF"
            }
            width: parent.width
            height: units.gu(5)
            onClicked: {
                PopupUtils.close(confirmSaveDialogue)
                mainPage.openedFileName = ""
                mainPage.currentFileLabelVisible = false
                textArea.text = ""
                mainPage.saved = true
            }
        }
    }
}
