/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3 as LI
import io.thp.pyotherside 1.4 //for Python
import Qt.labs.folderlistmodel 2.1 //for FolderListModel
import Qt.labs.platform 1.0 as PF //for StandardPaths
import Lomiri.Components.Popups 1.3 //for Dialogs and Popups

/*
Application settings pages about font and colours
*/
Page {
    id: settingsPage

    header: PageHeader {
        title: i18n.tr("Settings")
    }

    visible: false

    Flickable {
        id: settingsPageFlickable
        clip: true
        flickableDirection: Flickable.AutoFlickIfNeeded
        boundsBehavior: Flickable.StopAtBounds
        contentHeight: settingsColumn.childrenRect.height
        anchors {
            top: parent.top
            topMargin: units.gu(1)
            left: parent.left
            right: parent.right
            bottom: settingsPage.bottom
            bottomMargin: units.gu(1)
        }

        Column {
            id: settingsColumn
            anchors.fill: parent

            /* placeholder */
            Rectangle {
                color: "transparent"
                width: parent.width
                height: units.gu(5)
            }

            ListItem {
                divider.visible: false
                Label {
                    text: i18n.tr("General settings")
                    font.bold: true
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.topMargin: units.gu(1)
                }
            }

            LI.Standard {
                id: themePage
                height: themeSetting.height + units.gu(2)

                Label {
                    id: themeLabel
                    text: i18n.tr("App theme")
                    anchors {
                        leftMargin: units.gu(2)
                        left: parent.left
                        verticalCenter: parent.verticalCenter
                    }
                }

                ListModel {
                    id: themeModel
                    function initialize() {
                        themeModel.append({"text": i18n.tr("System"), "value": "System"})
                        themeModel.append({"text": i18n.tr("SuruDark"), "value": "SuruDark"})
                        themeModel.append({"text": i18n.tr("Ambiance"), "value": "Ambiance"})
                    }
                }

                OptionSelector {
                    id: themeSetting
                    anchors {
                        rightMargin: units.gu(2)
                        right: parent.right
                        verticalCenter: themeLabel.verticalCenter
                    }
                    width: units.gu(18)
                    model: themeModel
                    containerHeight: itemHeight * themeModel.count
                    delegate: OptionSelectorDelegate {
                        text: model.text
                        height: units.gu(4)
                    }
                    onDelegateClicked: {
                        settings.selectedTheme = model.get(index).value
                        root.setCurrentTheme();
                    }
                }
                Component.onCompleted: {
                    /*
                    The Component.onCompleted of the OptionSelector finishes BEFORE
                    the onCompleted events of the delegates or the model.
                    That is why the initialize() needs to be called here rather than
                    in the Component.onCompleted of the ListModel.
                    */
                    themeModel.initialize()
                    for (var i = 0; i < themeModel.count; ++i) {
                        if (themeModel.get(i).value === settings.selectedTheme) {
                            themeSetting.selectedIndex = i
                            return
                        }
                    }
                    themeSetting.selectedIndex = 0  // in case no match is found due to broken settings
                }
            }

            LI.Standard {
                id: pwdLengthSetting
                height: passwordLengthSelector.height + units.gu(2)

                Label {
                    id: passwordLengthLabel
                    text: i18n.tr("Minimum password length")
                    anchors {
                        leftMargin: units.gu(2)
                        left: parent.left
                        verticalCenter: parent.verticalCenter
                    }
                }

                ListModel {
                    id: pwdLengthModel
                    function initialize() {
                        //TRANSLATORS: %1 will be replaced with the number of characters
                        pwdLengthModel.append({"text": i18n.tr("%1 characters").arg(4), "value": 4})
                        pwdLengthModel.append({"text": i18n.tr("%1 characters").arg(6), "value": 6})
                        pwdLengthModel.append({"text": i18n.tr("%1 characters").arg(8), "value": 8})
                        pwdLengthModel.append({"text": i18n.tr("%1 characters").arg(10), "value": 10})
                    }
                }

                OptionSelector {
                    id: passwordLengthSelector
                    anchors {
                        rightMargin: units.gu(2)
                        right: parent.right
                        verticalCenter: passwordLengthLabel.verticalCenter
                    }
                    width: units.gu(18)
                    containerHeight: itemHeight * model.count
                    model: pwdLengthModel
                    delegate: OptionSelectorDelegate {
                        text: model.text
                        height: units.gu(4)
                    }
                    onDelegateClicked: settings.minPasswordLength = model.get(index).value
                }

                Component.onCompleted: {
                    pwdLengthModel.initialize()
                    for (var i = 0; i < pwdLengthModel.count; ++i) {
                        if (pwdLengthModel.get(i).value === settings.minPasswordLength) {
                            passwordLengthSelector.selectedIndex = i
                            return
                        }
                    }
                    // in case no match is found due to broken settings
                    passwordLengthSelector.selectedIndex = 0
                }
            }

            LI.Standard {

                Label {
                    id: encryptedHighlightLabel
                    text: i18n.tr("Highlight encrypted file names")
                    anchors {
                        leftMargin: units.gu(2)
                        left: parent.left
                        verticalCenter: parent.verticalCenter
                    }
                }

                Switch {
                    id: encryptedHighlight
                    checked: settings.highlightEncrypted
                    onCheckedChanged: settings.highlightEncrypted = checked

                    anchors {
                        rightMargin: units.gu(2)
                        right: parent.right
                        verticalCenter: parent.verticalCenter
                    }
                }
            }

            LI.Standard {

                Label {
                    id: startWithFileListLabel
                    text: i18n.tr("Open file list on app start")
                    anchors {
                        leftMargin: units.gu(2)
                        left: parent.left
                        verticalCenter: parent.verticalCenter
                    }
                }

                Switch {
                    id: startWithFile
                    checked: settings.startWithFileList
                    onCheckedChanged: settings.startWithFileList = checked

                    anchors {
                        rightMargin: units.gu(2)
                        right: parent.right
                        verticalCenter: parent.verticalCenter
                    }
                }
            }

            LI.Standard {

                Label {
                    id: clearCacheLabel
                    text: i18n.tr("Clear tedit app cache")
                    anchors {
                        leftMargin: units.gu(2)
                        left: parent.left
                        verticalCenter: parent.verticalCenter
                    }
                }

                Button {
                    id: clearCacheButton
                    color: theme.palette.normal.negative
                    width: buttonLabel.width + units.gu(2)
                    anchors {
                        rightMargin: units.gu(2)
                        right: parent.right
                        verticalCenter: parent.verticalCenter
                    }
                    onClicked: {
                        for (var i = 0; i < folderModel.count; i ++) {
                            py.call('os.remove', [folderModel.get (i, "fileURL").toString().replace("file://","")], function (result) {
                                //TODO: add error handling
                            });
                        }
                        PopupUtils.open(cacheClearedDialog)
                    }
                    Label {
                        id: buttonLabel
                        text: i18n.tr("Clear cache")
                        anchors.centerIn: parent
                        color: theme.palette.normal.raised
                    }
                }
            }

            ListItem {
                divider.visible: false
                Label {
                    text: i18n.tr("Notes area configuration")
                    font.bold: true
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.topMargin: units.gu(1)
                }
            }

            LI.Standard {

                Label {
                    id: enableWordWrapLabel
                    text: i18n.tr("Enable word wrap")
                    anchors {
                        leftMargin: units.gu(2)
                        left: parent.left
                        verticalCenter: parent.verticalCenter
                    }
                }

                Switch {
                    id: wrap
                    checked: settings.wordWrap
                    text: i18n.tr("Word wrap")
                    onCheckedChanged: {
                        settings.wordWrap = checked
                        if (checked)
                            textArea.wrapMode = TextEdit.Wrap
                        else
                            textArea.wrapMode = TextEdit.NoWrap
                    }

                    anchors {
                        rightMargin: units.gu(2)
                        right: parent.right
                        verticalCenter: parent.verticalCenter
                    }
                }
                onClicked: wrap.checked = !wrap.checked
            }

            ListItem {
                id:fontSizeContainer
                height: units.gu(8)
                Label {
                    id:fontLabel
                    text: i18n.tr("Notes font size")+": "+textArea.font.pixelSize
                    anchors {
                        leftMargin: units.gu(2)
                        left: parent.left
                        verticalCenter: parent.verticalCenter
                    }
                }

                Slider {
                    id: slider
                    function formatValue(v) { return v.toFixed(1) }
                    minimumValue: 20
                    maximumValue: 80
                    value: settings.pixelSize
                    live: false
                    onValueChanged: {
                        textArea.font.pixelSize = formatValue(value) * 1
                        settings.pixelSize = formatValue(value) * 1
                    }

                    anchors {
                        leftMargin: units.gu(3)
                        left: fontLabel.right
                        rightMargin: units.gu(2)
                        right: parent.right
                        verticalCenter: parent.verticalCenter
                    }
                }
            }

            ListItem {
                divider.visible: false
                Label {
                    text: i18n.tr("Notes area font color")
                    font.bold: true
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.topMargin: units.gu(1)
                }
            }

            ColorListItem {
                id: greenFont
                label: i18n.tr("Green")
                itemColor: theme.palette.normal.positive
                onClicked: {
                    settings.textAreaFontColor = itemColor
                    textArea.color = itemColor
                    pageStack.pop()
                }
            }

            ColorListItem {
                id: yellowFont
                label: i18n.tr("Yellow")
                itemColor: "gold"
                onClicked: {
                    settings.textAreaFontColor = itemColor
                    textArea.color = itemColor
                    pageStack.pop()
                }
            }

            ColorListItem {
                id: greyFont
                label: i18n.tr("Grey")
                itemColor: "grey"
                onClicked: {
                    settings.textAreaFontColor = itemColor
                    textArea.color = itemColor
                    pageStack.pop()
                }
            }

            ColorListItem {
                divider.visible: false
                id: blackFont
                label: i18n.tr("Black/white (theme dependend)")
                itemColor: theme.palette.normal.foregroundText
                onClicked: {
                    settings.textAreaFontColor = "theme font color"
                    textArea.color = itemColor
                    pageStack.pop()
                }
            }
            ListItem {
                id: fakeDivider
                divider.visible: true
                height: divider.height
            }

            /* ------------ Application background colors -------------- */

            ListItem {
                divider.visible: false
                Label {
                    text: i18n.tr("Note page background color")
                    font.bold: true
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.topMargin: units.gu(1)
                }
            }

            ColorListItem {
                id: lightColorChooser
                label: i18n.tr("Light blue")
                itemColor: '#126381'
                onClicked: {
                    settings.pageBackgroundColor = itemColor
                    pageStack.pop()
                }
            }

            ColorListItem {
                id: darkBlueColorChooser
                label: i18n.tr("Dark blue")
                itemColor: '#1E2F3F'
                onClicked: {
                    settings.pageBackgroundColor = itemColor
                    pageStack.pop()
                }
            }

            ColorListItem {
                id: brownColorChooser
                label: i18n.tr("Brown")
                itemColor: "saddlebrown"
                onClicked: {
                    settings.pageBackgroundColor = itemColor
                    pageStack.pop()
                }
            }

            ColorListItem {
                id: whiteColorChooser
                label: i18n.tr("Black/white (theme dependend)")  /* Default */
                itemColor: theme.palette.normal.background
                onClicked: {
                    settings.pageBackgroundColor = "theme background color"
                    pageStack.pop()
                }
            }
        } //col

    } //Flickable

    Scrollbar {
        flickableItem: settingsPageFlickable
        align: Qt.AlignTrailing
    }

    Component {
        id: cacheClearedDialog

        Dialog {
            id: cacheDialog
            title: i18n.tr("Cache cleared.")

            Label {
                id: messageLabel
                text: messagetext
            }

            Button {
                text: i18n.tr("OK")
                onClicked: PopupUtils.close(cacheDialog)
            }
        }
    }

    //initiate a Python component for calls to Python commands
    Python {
      id: py

      Component.onCompleted: {
          //this works as the import statement in a python script
          importModule('os', function() { console.log("DEBUG: python loaded"); });
      }
    }
    //used to read all files from the apps .cache folder
    FolderListModel {
      id: folderModel
      folder: PF.StandardPaths.writableLocation(PF.StandardPaths.CacheLocation) + "/qmlcache"
      nameFilters: ["*.qmlc","*.jsc"]
    }
}
