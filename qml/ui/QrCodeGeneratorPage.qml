/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

import "../components"
import "../js/qqr.js" as QRCodeBackend

/*
Page where is shown the QR Code associated at the chosen url
*/
Page {
    id: qrCodeGeneratorPage
    /* the url to represent with a QR Code */
    property string qrcodeContent;

    anchors.fill: parent
    visible: false

    header: PageHeader {
        id: header
        title: i18n.tr("QR code")
    }

    QRCode {
        id: qrcodeComponent
        anchors.top : header.bottom
        anchors.topMargin : units.gu(4)
        anchors.horizontalCenter : parent.horizontalCenter

        width : parent.width - units.gu(4)
        height : parent.height - units.gu(4)
        value : qrcodeContent
        level : "H"
    }

    Label {
        id: contentLabel
        //TRANSLATORS: %1 will be replaced with the text content of the QR code displayed above this label
        text: i18n.tr("Content: %1").arg(qrcodeContent)
        width: parent.width
        anchors.bottom : parent.bottom
        anchors.bottomMargin : units.gu(2)
        anchors.left: parent.left
        anchors.leftMargin : units.gu(4)
        anchors.right: parent.right
        anchors.rightMargin : units.gu(4)
        wrapMode: Text.Wrap
    }
}
