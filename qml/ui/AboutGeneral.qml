/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3

Item {
    id: aboutGeneralPage

    Flickable {
        id: page_flickable

        flickableDirection: Flickable.AutoFlickIfNeeded

        anchors.fill: parent
        contentHeight:  contentColumn.height + units.gu(2)
        clip: true

        Column {
            id: contentColumn
            width: parent.width

            Item {
                id: icon

                width: parent.width
                height: app_icon.height + units.gu(4)
                anchors.horizontalCenter: parent.horizontalCenter

                LomiriShape {
                    id: app_icon

                    width: Math.min(aboutGeneralPage.width/3, 256)
                    height: width
                    anchors.centerIn: parent

                    source: Image {
                        id: icon_image
                        source: Qt.resolvedUrl("../../assets/tedit.png")
                    }
                    radius: "small"
                    aspect: LomiriShape.DropShadow
                }
            }

            Label {
                id: name

                text: i18n.tr("tedit") + " v%1".arg(Qt.application.version)
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottomMargin: units.gu(4)
                textSize: Label.Large
                horizontalAlignment:  Text.AlignHCenter
            }

            Repeater {
                id: aboutLinks

                model: [
                    { name: i18n.tr("Source code"), url: "https://gitlab.com/Danfro/tedit" },
                    { name: i18n.tr("License") + ": GNU GPLv3", url: "https://www.gnu.org/licenses/quick-guide-gplv3.html" },
                    { name: i18n.tr("tedit in OpenStore"), url: "https://open-store.io/app/danfro.tedit" },
                    { name: i18n.tr("Report bugs"),  url: "https://gitlab.com/Danfro/tedit/issues" },
                    { name: i18n.tr("Translate on Weblate"), url: "https://hosted.weblate.org/projects/ubports/tedit/" },
                    { name: i18n.tr("Changelog"), url: "https://gitlab.com/Danfro/tedit/-/blob/main/CHANGELOG.md" }
                ]

                delegate: ListItem {
                    divider { visible: false; }
                    height: layoutAbout.height
                    ListItemLayout {
                        id: layoutAbout
                        title.text : modelData.name
                        ProgressionSlot {name: "external-link"; }
                    }
                    onClicked: Qt.openUrlExternally(modelData.url)
                }
            }

            Rectangle {
                // spacer
                width: parent.width
                height: units.gu(2)
                color: "transparent"
            }

            Repeater {
                id: donateLinks

                model: [
                    { name: i18n.tr("Donate to UBports"), subtitle: i18n.tr("UBports supports the development of Ubuntu Touch."), url: "https://ubports.com/en_EN/donate" },
                    { name: i18n.tr("Donate to me"), subtitle: i18n.tr("I am maintaining and improving this app."),url: "https://paypal.me/payDanfro" }
                ]

                delegate: ListItem {
                    divider { visible: false; }
                    height: layoutAbout.height
                    ListItemLayout {
                        id: layoutAbout
                        title.text : modelData.name
                        subtitle.text: modelData.subtitle
                        ProgressionSlot {name: "external-link"; }
                    }
                    onClicked: Qt.openUrlExternally(modelData.url)
                }
            }
        }
    }
}
