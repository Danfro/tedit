/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3 as ListItem
import Lomiri.Components.Popups 1.3
import Lomiri.Content 1.1
import "../components"

/*
  Page that list all the locally saved files.
  Allows to search, open or delete files or display file infos.
  A list item swipe is used for some actions.
*/
Page {
    id: localFilePickerPage
    visible: false

    /* info about the currently selected file in the listModel */
    property string selectedFileSize: ""
    property string selectedFileModificationDate: ""
    property string selectedFileIndex: ""

    Component {
        id: fileInfoPopUp
        FileInfoPopUp{}
    }

    Component {
        id: confirmDeleteFileDialog
        RemoveFileDialog{imageListModelIndex: localFilePickerPage.selectedFileIndex}
    }

    Component {
        id: decryptKeyInput
        DecryptKeyInput {fileListModelIndex: localFilePickerPage.selectedFileIndex}
    }

    /* the list of locally saved files */
    ListModel {
        id: localFileslistModel
    }

    /*
    Ask confirmation before removing all locally saved files
    */
    Component {
        id: confirmComponent
        Dialog {
            id: confirmDialogue
            title: i18n.tr("Attention")
            text: i18n.tr("Remove ALL saved files? (No restore possible)")

            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: units.gu(2)

                Button {
                    text: i18n.tr("Close")
                    onClicked: PopupUtils.close(confirmDialogue)
                    width: units.gu(14)
                }

                Button {
                    id: removeButton
                    text:  i18n.tr("Delete")
                    color: theme.palette.normal.negative
                    width: units.gu(14)
                    onClicked: {
                        var fileList = fileIO.getLocalFileList(root.fileSavingPath);
                        for(var i=0; i<fileList.length; i++) {
                            fileIO.remove(root.fileSavingPath + fileList[i])
                            /* console.log("Deleting file with name: "+fileList[i]); */
                            localFileslistModel.clear();
                        }
                        /* restore as first time use */
                        mainPage.saved = true /* ie: file NOT modified yet */
                        mainPage.openedFileName = "";
                        mainPage.currentFileLabelVisible = false;
                        textArea.text = "";

                        deleteOperationResult.text = i18n.tr("All Files successfully removed")
                        removeButton.enabled = false
                        removeButton.visible = false
                    }
                }
            }

            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                Label {
                    text: " "
                    id: deleteOperationResult
                    font.bold: true
                }
            }
        }
    }

    header: PageHeader {
        id: pageHeader
        height: units.gu(11)

        trailingActionBar.actions: [
            Action {
                id: deleteAll
                text: i18n.tr("Delete all")
                iconName: "delete"
                onTriggered: PopupUtils.open(confirmComponent)
            }
        ]

        contents: Loader {
            id: searchComponentLoader
            sourceComponent: searchComponent
            anchors {
                left: parent ? parent.left : undefined
                right: parent ? parent.right : undefined
                verticalCenter: parent ? parent.verticalCenter : undefined
            }

            Label {
                id: infoLabel
                text: i18n.tr("found %1 files, swipe an entry for file info and delete options").arg(localFileslistModel.count)
                wrapMode: Text.Wrap
                font.italic: true
                anchors {
                    top: searchComponentLoader.bottom
                    topMargin: units.gu(1)
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                    bottomMargin: units.gu(1)
                }
            }
        }
    }

    /* when the page become active, load the locally saved files in the ListModel */
    onActiveChanged: {
        if (active) {
            loadFileList()
        }
    }

    function loadFileList() {
        var fileList = fileIO.getLocalFileList(root.fileSavingPath);
        localFileslistModel.clear();

        for (var i=0; i<fileList.length; i++) {
            var targetFileName = "file://" + root.fileSavingPath + fileList[i];
            var fileSize = fileIO.getSize(targetFileName); //bytes
            var lastModifiedDate = fileIO.getFileLastModified(targetFileName);
            if (searchComponentLoader.item.text.trim() === "") {
                localFileslistModel.append({"file": fileList[i]});
            } else {
                // use regex with i flag for case-insensitive search
                if (fileList[i].search(new RegExp(searchComponentLoader.item.text, 'i')) != -1) {
                    localFileslistModel.append({"file": fileList[i]});
                }
            }
        }
    }

    Timer {
        id: searchTimer
        interval: 250
        onTriggered: loadFileList()
    }

    Component {
        id: searchComponent
        TextField {
            id: searchField
            objectName: "searchField"

            inputMethodHints: Qt.ImhNoPredictiveText
            placeholderText: i18n.tr("Search files")
            hasClearButton: true

            onTextChanged: searchTimer.restart()
        }
    }

    /* show the list of locally saved files in the App folder */
    LomiriListView {
        id: lomiriListView
        anchors {
            top: pageHeader.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: units.gu(1)
        }
        model: localFileslistModel
        delegate: SavedFilesListDelegate {}
    }

    Component.onCompleted: searchComponentLoader.item.forceActiveFocus() // activate search field and OSK when the page is loaded
}
