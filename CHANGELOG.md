v3.5.0
- added: search option for file list
- added: setting to open the file list on app start
- added: option to clear the apps cache
- added: arrow button to toggle toolbar on and off
- improved: importing a file which already exists, now gets a timestamp added to avoid overwriting
- fixed: occasional crashing when trying to copy or paste or switching apps
- fixed: highlight encrypted now doesn't also change wrap setting
- updated: translations, many thanks to all translators!

v3.4.0
- added: toolbar for quick access to several commands from the menu, reordered some menu entries to match button order
- added: exit button which checks for unsaved changes, some header redesign related to this
- added: option to highlight file names of excrypted files in file list
- improved: excrypted files in file list are now marked with the padlock icon
- improved: file name in main screen now shows storage path
- improved: change icon to view passwort for the common eye icon
- improved: edit file button behaviour and design in file list
- improved: note on file storage now gives some more information
- improved: several input fields now get focus when opening the page, thus opening the OSK
- improved: fix #13 by rephrasing some error messages to be more helpful for the user
- improved: some buttons with long text are now bigger and use word wrap
- improved: readability of save dialog button in disabled state
- removed: string "saved: true/false" since the label color  and save button enabled/disabled indicate saved state
- fixed: top margin at file list page being a bit too small
- fixed: error in hash calculator where proceed wouldn't work
- updated: translations, many thanks to all translators!

v3.3.0
- fixed: #10 empty password and empty file name allow to press save button
- added: setting for minimum password length
- added: setting for theme selection
- added: import from note button for base64 coder
- added: warning dialogs if there is unsaved content on new note, website import and base64 import
- improved: added a network available check for website import
- improved: added a simple error message when import of website fails
- improved: notes can now be opended with a icon in the list, swipe is not needed anymore
- improved: qr code background works now with themes and show content at bottom to allow longer strings
- improved: hash calculator page now uses normal themed colors
- improved: save handling of base64 coder
- updated: translations, many thanks to all translators!

v.3.2.0
- fixed: #1 notes page background color is now persistent
- added: #5 clear clipboard option
- added: infomessage, when trying to paste from clibpoard and clipboard is empty
- improved: edit area now resizes height when OSK is visible
- improved: all pages follow system theme now, except custom main pages background
- improved: settings page design, font size setting moved up
- improved: password dialogs now hide the password characters
- fixed: place checkboxes on left side of label
- removed: word suggestion setting, this can be configured in system settings
- updated: translations, many thanks to all translators!

v3.1.0
- added: copy button in header to base64 encoder page
- added: tedit as target for .txt files to be shared via contenthub, handling of contenthub imported .txt files
- added: option to add .txt extension to files when saving, enabled by default
- improved: add OK button to info dialogs for closing them
- improved: rename "open" to "open file list"
- improved: copy hash/base64 to note now adds a line break before the hash
- improved: remove file confirmation dialog no longer needs an extra close to be pressed
- fixed: #4 base64 encoder and digest calculator not respecting set font size
- updated: translations, many thanks to all translators!

Known bug: background color is not persistent after consecutive starts of the app

v3.0.0
- new maintainer @danfro, moved the app repository to Gitlab
- improved: released for focal
- improved: redesign, use Ubuntu Touch system icons in most places to make the app look more native
- improved: use menu page instead of menu dialog
- improved: about page redesign
- improved: flick behaviour of some pages
- improved: some spelling corrections
- improved: use themed colors where possible
- improved: replaced hardcoded file storage path with system path
- added: CI script for automatic builds in Gitlab
- added: translation now on hosted weblate
- added: yellow font color
- fixed: black font color on black background and yellow on white background not possible anymore
- fixed: outline of text area border 
- fixed: confirming "clear note" does now close the dialog
- updated: translations, many thanks to all translators!


